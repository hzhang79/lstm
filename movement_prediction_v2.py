
print("setting up backend, please wait")
# import datetime
import socket
import pandas as pd
import os
import time
import warnings
import numpy as np
from numpy import newaxis
import math
import pandas
import matplotlib
import matplotlib.pyplot as plt
import csv
from datetime import datetime

from keras.layers.core import Dense, Activation, Dropout
from keras.layers.recurrent import LSTM
from keras.models import Sequential

from sklearn.metrics import mean_squared_error



"""
IQ DTN Feed Historical Symbol Download.
Downloads the symbol data in CSV format and stores it in a local directroy.
If we already have the symbol data downloaded, it will not hit IQ DTN Feed again,
it will simple use the local data.
To flush the local CSV file, simply delete the directory.

Constructor enables to specify a start and end date for the symbol data as well
as the frequency. Great for making sure data is consistent.

Simple usage example:

    from iqfeed import historicData

    dateStart = datetime.datetime(2014,10,1)
    dateEnd = datetime.datetime(2015,10,1)        
        
    iq = historicData(dateStart, dateEnd, 60)
    symbolOneData = iq.download_symbol(symbolOne)
    
"""
class historicData:

    def __init__(self, startDate, endDate, timeFrame=60):
        
        self.startDate = startDate.strftime("%Y%m%d %H%M%S")
        self.endDate = endDate.strftime("%Y%m%d %H%M%S")
        self.timeFrame = str(timeFrame)
        self.host = "127.0.0.1"  # Localhost
        self.port = 9100  # Historical data socket port

    def read_historical_data_socket(self, sock, recv_buffer=4096):
        """
        Read the information from the socket, in a buffered
        fashion, receiving only 4096 bytes at a time.
    
        Parameters:
        sock - The socket object
        recv_buffer - Amount in bytes to receive per read
        """
        buffer = ""
        data = ""
        while True:
            data = sock.recv(recv_buffer).decode('utf-8')
            buffer += data
    
            # Check if the end message string arrives
            if "!ENDMSG!" in buffer:
                break
       
        # Remove the end message string
        buffer = buffer[:-12]
        return buffer
        
    def download_symbol(self, symbol):

  
            
        message = "HIT,{0},{1},{2},{3},,093000,160000,1\n".format(symbol, int(self.timeFrame), self.startDate, self.endDate)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.host, self.port))
        sock.sendall(message.encode('utf-8'))
        data = self.read_historical_data_socket(sock)
        sock.close

        data_split_rows = data.split("\r")
        data_list = []
        for row in data_split_rows:
            split_row = row.replace("\n","").split(",")[:-1]
            if len(split_row) != 0:
                data_list.append(split_row)

        #INITIALIZE TS DATAFRAME

        ts_df = pd.DataFrame(data_list,columns=['date','high','low','open','close','volume','oi'])
        
        ts_df['date'] = pd.to_datetime(ts_df['date'])
        ts_df['date'] = pd.DatetimeIndex(ts_df.date).normalize()
        ts_df['date'] = ts_df['date'].astype(str)
        ts_df[['high','low','open','close','volume','oi']] = ts_df[['high','low','open','close','volume','oi']].apply(pd.to_numeric)
        
        #INITIALIZE TH DATAFRAME
        th_df = ts_df[['date', 'close']].copy()
            
        return ts_df, th_df

##############################
# LSTM 
###########################

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' #Hide messy TensorFlow warnings
warnings.filterwarnings("ignore") #Hide messy Numpy warnings


def load_data(dataframe, seq_len, normalise_window):

    data = dataframe["close"].tolist()
    sequence_length = seq_len + 1
    result = []
    for index in range(len(data) - sequence_length):
        result.append(data[index: index + sequence_length])

    #if there are 1000 obs, it will produce
    #0 to 968
    #31 to 999
    #miss out the last data point
    if normalise_window:
        result = normalise_windows(result)

    result = np.array(result)
    row = round(0.9 * result.shape[0])
    train = result[:int(row), :]
    np.random.shuffle(train)
    x_train = train[:, :-1]
    y_train = train[:, -1]
    x_test = result[int(row):, :-1]
    y_test = result[int(row):, -1]
    x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], 1))
    x_test = np.reshape(x_test, (x_test.shape[0], x_test.shape[1], 1))
    return [x_train, y_train, x_test, y_test]

def normalise_windows(window_data):
    normalised_data = []
    for window in window_data:
        normalised_window = [((float(p) / float(window[0]))-1) for p in window]
        normalised_data.append(normalised_window)
    return normalised_data

def build_model(layers):
    model = Sequential()

    model.add(LSTM(
        input_shape=(layers[1], layers[0]),
        output_dim=layers[1],
        return_sequences=True))
    model.add(Dropout(0.2))

    model.add(LSTM(
        layers[2],
        return_sequences=False))
    model.add(Dropout(0.2))

    model.add(Dense(
        output_dim=layers[3]))
    model.add(Activation("linear"))

    start = time.time()
    model.compile(loss="mse", optimizer="rmsprop")
    print("> Compilation Time : ", time.time() - start)
    return model

def predict_point_by_point(model, data):
    #Predict each timestep given the last sequence of true data, in effect only predicting 1 step ahead each time
    predicted = model.predict(data)
    predicted = np.reshape(predicted, (predicted.size,))
    return predicted

def predict_sequence_full(model, data, window_size):
    #Shift the window by 1 new prediction each time, re-run predictions on new window
    curr_frame = data[0]
    predicted = []
    for i in range(len(data)):
        predicted.append(model.predict(curr_frame[newaxis,:,:])[0,0])
        curr_frame = curr_frame[1:]
        curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
    return predicted

def predict_sequences_multiple(model, data, window_size, prediction_len):
    prediction_seqs = []
    for i in range(int(len(data)/prediction_len)+1):
        curr_frame = data[i*prediction_len]
        predicted = []
        for j in range(prediction_len):
            predicted.append(model.predict(curr_frame[newaxis,:,:])[0,0])
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(curr_frame, [window_size-1], predicted[-1], axis=0)
        prediction_seqs.append(predicted)
    return prediction_seqs




def plot_results(predicted_data, true_data):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.legend()
    plt.show()

def plot_results_multiple(predicted_data, true_data, prediction_len):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    #Pad the list of predictions to shift it in the graph to it's correct start
    for i, data in enumerate(predicted_data):
        padding = [None for p in range(i * prediction_len)]
        plt.plot(padding + data, label='Prediction')
        plt.legend(loc=1,prop={'size':5})
    plt.show()

    
def run_lstm(ticker, dateStart, dateEnd):
    global_start_time = time.time()
    iq = historicData(dateStart, dateEnd, 46800)
    ts_df, th_df = iq.download_symbol(ticker)
    
    seq_len = 30
    pred_len = 10
    
    X_train_actual, y_train_actual, X_test_actual, y_test_actual = load_data(th_df, seq_len, False)
    len_act = len(y_test_actual)
    
    print('> Loading data... ')
    X_train, y_train, X_test, y_test = load_data(th_df, seq_len, True)
    print('> Data Loaded. Compiling...')

    Y_test_actual=[]
    for n in y_test_actual:
        Y_test_actual.append(n)
        
    prediction_list=[]        
    epoch_list=[15,30,60,90]
    mse_list=[]
    for i in epoch_list:
        model = build_model([1, seq_len, 100, 1])
        model.fit(
             X_train,
             y_train,
             batch_size=512,
             nb_epoch=i,
             validation_split=0.05)
        predictions = predict_sequences_multiple(model, X_test, seq_len, pred_len)
        pred_seq = []
        for i in predictions:
            for j in i:
                pred_seq.append(j)
        mse_list.append(mean_squared_error(Y_test_actual,pred_seq[0:len_act]))
        prediction_list.append(predictions)
        
    min_mse_epoch=epoch_list[mse_list.index(min(mse_list))]
    min_mse_prediction=prediction_list[mse_list.index(min(mse_list))]
    print(mse_list)
    return global_start_time, min_mse_prediction,y_test, pred_len, X_test_actual, y_test_actual, ts_df

##############################################################
# PREDICTION
#################################################################

def convert_pred_to_price(pred_seq, x_test_actual, pred_len):
    pred_price = []
    for i in range(int(len(pred_seq)/pred_len)):
        x_test_to_use = float(np.asscalar(X_test_actual[i*pred_len][0]))
        window = pred_seq[i*pred_len:(i+1)*pred_len]
        actual_price = [(float(p)+1)*x_test_to_use for p in window]
        pred_price.append(actual_price)
    return pred_price

def plot_price_multiple(pred_price, actual_price):
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(actual_price, label='True Data')
    plt.plot(pred_price, label='Prediction')
    plt.legend(loc=2,prop={'size':9})
    plt.show()
       
def make_prediction(ticker, global_start_time, min_mse_prediction,y_test, pred_len, X_test_actual, y_test_actual, ts_df):

    final_filename= ticker + "final.csv"
    pred_filename=ticker + "pred.csv"
    actual_filename=ticker + "actual.csv"

    #trend estimate in the long run
    print('Training duration (s) : ', time.time() - global_start_time)
    plot_results_multiple(min_mse_prediction, y_test, pred_len)

    #since we used seq_len period return data above, we need to convert it to price to draw
    #the predictio graph
    #for it to be more clear

    pred_seq = []
    for i in min_mse_prediction:
        for j in i:
            pred_seq.append(j)



    pred_price = convert_pred_to_price(pred_seq, X_test_actual,pred_len)
    pred_pr =[]
    for i in pred_price:
        for j in i:
            pred_pr.append(j)


    plot_price_multiple(pred_pr, y_test_actual)

    df = pandas.DataFrame(None)
    df['pred']=pred_pr
    df.to_csv(pred_filename)

    df1 = pandas.DataFrame(None)
    df1['actual']=y_test_actual
    df1.to_csv(actual_filename)
    #write csv to draw graph in r
    #take these two files and the original price file straight from DTN to run in R



    total_len = ts_df.shape[0]
    len_act = len(y_test_actual)

    date = ts_df.iloc[(total_len - len_act):total_len]['date']
    date = date.tolist()
    final_date=[]
    for j in date:
    #     j = datetime.strptime(j, '%m/%d/%Y')
        j = datetime.strptime(j, '%Y-%m-%d')
        final_date.append(j)


    final_file=pandas.DataFrame(None)
    final_file['date'] = final_date
    final_file['actual'] = y_test_actual
    final_file['pred'] = pred_pr[0:len_act]

    final_file.index.name=None
    final_file.to_csv(final_filename,index=False)

print('finished setting up backend, thank you!')

tickers = [x for x in input("Please enter a list of tickers separated by a space(for example: @KC# @S#:     ").split()]
print ("you entered", tickers)
print("")
dateStart = datetime.strptime(input("Please enter the start date (in format 2013-10-08):"), '%Y-%m-%d')
print ("you entered", dateStart)
print("")
dateEnd = datetime.strptime(input("Please enter the end date (in format 2017-09-26):"), '%Y-%m-%d')
print ("you entered", dateEnd)
print("")

#Main Run Thread
if __name__=='__main__':
    
    for ticker in tickers:
        print("MAKING PREDICTION FOR: {}".format(ticker))
        global_start_time, min_mse_prediction,y_test, pred_len, X_test_actual, y_test_actual, ts_df = run_lstm(ticker, dateStart, dateEnd)
        make_prediction(ticker, global_start_time, min_mse_prediction,y_test, pred_len, X_test_actual, y_test_actual, ts_df)
