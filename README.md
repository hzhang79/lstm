# FVP Long Short Term Memory #

### What is this repository for? ###

This program takes 3 inputs from the user, first a list of ticker symbols (for example @KC#), second a start date and thirdly an end date.
With these inputs the program makes the appropreate requests to the DTN IQFeed for the relavant data, then formats it into pandas dataframes.
Afterward it run the LSTM algorithm and trains it with the data returned from DTN.
With this trained machine learing algorithm the program finally moves on to makeing predictions, graphs them and outputs 3 CSVs (with the actual data, the predicted data and the final data).

Version 2.0

### How do I get set up? ###
This programs requires the installation of Python 3.6 with the following packages:

	socket,
	pandas,
	warnings,
	numpy,
	matplotlib,
	keras,
	sklearn,
	jupyter notebook (that is included in the anaconda instalation)

	NOTE: if any of the previous python packages are missing. just open up a shell and input;
	$ pip install <NAME OF PACKAGE>

It also requires the installation of the DTN IQLauncher, which is installed with the DTN Client. It is also necessary to have an authorized DTN User and Password. 

STEPS TO RUN:
1) Open DTN IQLauncher and login

2) Open an Anaconda terminal (or a Windows Command terminal if you have Python installed directly)

3) Navigate to the working folder (if cloned from Bitbucket it should be something like ../fvp_lstm)

4) 
	
	4.1) run the program (There are 2 versions):
		
		4.11) The first is a plain python file (movement_prediction_v2.py) 
			It can be run from the console (in the appropreate directory) with:
		
			$python movement_prediction_v2.py

		!!!!RECOMENDED VERSION!!!
		4.12) The second version is a Jupyter Notebook (movement_prediction_v2.ipynb).
		That can be run from the console (in the appropreate directory) with:

		$jupyter notebook

		This will automatically open the directory in your browser, click on movement_prediction_v2.ipynb.
		you can either:
		click on the first cell, and press "SHIFT + ENTER" to run each successive cell.
		NOTE: please wait that the previous cell has finished running (in the brackets to left there is a number, not "*"). Or else you will get an error.
		or:
		click the "kernel" button on the toolbar and select "Restart & Run All" from the dropdown menu.
	
	4.2) FILL OUT OPTIONS PROMPTED IN PROGRAM:
		The program will ask you to fill out the specified tickers and the start and end date.

### Contribution guidelines ###

if you wish to contribute please leave a cristal clear comment in your commit.

### Who do I talk to? ###

If you have any issues you can contact Diego Raggio via the following methods:
	Skype: diego.raggio
	Email: didiraggio@gmail.com